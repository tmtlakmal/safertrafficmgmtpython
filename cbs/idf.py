from itertools import combinations


import argparse
import yaml
import numpy as np
from gridworld import VertexState, EdgeState, Grid, AgentData, Plan
import astar as astar
import cbs as cbs


class IllegalMoveTable(object):
    def __init__(self, plan):
        self.plan = plan
        self.vertex_constraints = dict()
        self.edge_constraints = dict()
        self.max_t = -1

    def is_illegal(self, current_vs, current_es):
        t = current_vs.time
        if t > self.max_t:
            self.vertex_constraints[t] = self.plan.get_conf_vertex_states(t)
            self.edge_constraints[t] = self.plan.get_conf_edge_states(t)
            self.max_t = t
        if current_vs in self.vertex_constraints[t] or current_es in self.edge_constraints[t]:
            return True
        return False


class ConflictAvoidanceTable(object):
    def __init__(self, plan):
        self.plan = plan
        self.vertex_constraints = dict()
        self.edge_constraints = dict()
        self.max_t = -1

    def get_conflict_count(self, agent, current_vs, current_es):
        t = current_vs.time
        if t > self.max_t:
            self.vertex_constraints[t] = self.plan.get_conf_vertex_state_dict(t)
            self.edge_constraints[t] = self.plan.get_conf_edge_state_dict(t)
            self.max_t = t
        c = 0
        if current_vs in self.vertex_constraints[t].keys():
            vas = self.vertex_constraints[t][current_vs]
            if agent not in vas:
                c += len(vas)
            else:
                c += len(vas) - 1
        if current_es in self.edge_constraints[t].keys():
            eas = self.edge_constraints[t][current_es]
            if agent not in eas:
                c += len(eas)
            else:
                c += len(eas) - 1
        return c


class Group(object):
    def __init__(self, grid, agents):
        self.grid = grid
        self.agents = agents
        self.plan = cbs.search(self.grid, self.agents)
        self.imt = IllegalMoveTable(self.plan)

    def replan(self, imt):
        plan = cbs.search(self.grid, self.agents, self.plan.get_cost(), imt)
        if plan:
            self.plan = plan
            self.imt = IllegalMoveTable(self.plan)
            return True
        return False

    def __eq__(self, other):
        return self.agents == other.agents

    def __hash__(self):
        return hash(str(self.agents))


class GroupConflict(object):
    def __init__(self, g1: Group, g1_state, g2: Group, g2_state):
        self.g1_state = g1_state
        self.g2_state = g2_state
        self.g1 = g1
        self.g2 = g2

    def __eq__(self, other):
        return self.g1 == other.g1 and self.g2 == other.g2

    def __hash__(self):
        return hash((self.g1, self.g2))


class IDF(object):
    def __init__(self, grid, agents):
        self.grid = grid
        self.agents = agents
        self.groups = set()
        for agent in self.agents:
            g = Group(grid, [agent])
            self.groups.add(g)
        self.cat = ConflictAvoidanceTable(self.combine_solution())

    def indepence_detection(self):
        conflicts = set()
        while True:
            conflict = self.get_first_conflict()
            if conflict:
                if conflict in conflicts:
                    conflicts.remove(conflict)
                    self.merge(conflict.g1, conflict.g2)
                else:
                    conflicts.add(conflict)
                    if not conflict.g1.replan(conflict.g2.imt):
                        if not conflict.g2.replan(conflict.g1.imt):
                            conflicts.remove(conflict)
                            self.merge(conflict.g1, conflict.g2)
                self.cat = ConflictAvoidanceTable(self.combine_solution())
            else:
                break
        return self.combine_solution()

    def get_max_t(self):
        return max([group.plan.get_max_t() for group in self.groups])

    def get_first_conflict(self) -> GroupConflict:
        if len(self.groups) < 2:
            return False
        for t in range(self.get_max_t()):
            v_t = {}
            e_t = {}
            for group in self.groups:
                for agent, path in group.plan.agent_paths.items():
                    vs = group.plan.get_state(agent, t)
                    es = group.plan.get_edge_state(agent, t)
                    if vs.get_conf_state() in v_t.keys():
                        v_g = v_t[vs.get_conf_state()]
                        if v_g != group:
                            return GroupConflict(group, vs, v_g, vs.get_conf_state())
                    else:
                        v_t[vs] = group
                    if es.get_conf_state() in e_t.keys():
                        e_g = e_t[es.get_conf_state()]
                        if e_g != group:
                            return GroupConflict(group, es, e_g, es.get_conf_state())
                    else:
                        e_t[es] = group
        return False

    def merge(self, g1:Group, g2:Group):
        self.groups.remove(g1)
        self.groups.remove(g2)
        merged = []
        merged.extend(g1.agents)
        merged.extend(g2.agents)
        merged = Group(self.grid, merged)
        self.groups.add(merged)

    def combine_solution(self):
        plan = Plan()
        for group in self.groups:
            plan.add_paths(group.plan)
        return plan


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("param", help="input file containing map and obstacles")
    parser.add_argument("output", help="output file with the schedule")
    args = parser.parse_args()

    # Read from input file
    with open(args.param, 'r') as param_file:
        try:
            param = yaml.load(param_file, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)

    dimension = param["map"]["dimensions"]
    obstacles = param["map"]["obstacles"]
    agents_data = param['agents']

    grid = Grid(dimension[0], dimension[1], obstacles)
    agents = AgentData.create(dimension, agents_data)

    # Searching
    idf = IDF(grid, agents)
    plan = idf.indepence_detection()
    solution = plan.generate_plan(grid)
    if not solution:
        print(" Solution not found")
        return

    # Write to output file
    with open(args.output, 'r') as output_yaml:
        try:
            output = yaml.load(output_yaml, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)

    output["schedule"] = solution
    output["cost"] = 0
    with open(args.output, 'w') as output_yaml:
        yaml.safe_dump(output, output_yaml)


if __name__ == "__main__":
    main()
