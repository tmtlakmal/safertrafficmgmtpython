import sys

sys.path.insert(0, '../')
import argparse
import yaml
import time
from itertools import combinations
from copy import deepcopy
from gridworld import VertexState, Grid, AgentData, Plan
import astar as astar
from astar import AStarConfig


class Conflict(object):
    VERTEX = 1
    EDGE = 2

    def __init__(self, type, agent_1, agent_1_state, agent_2, agent_2_state):
        self.type = type
        self.agent_1 = agent_1
        self.agent_2 = agent_2
        self.agent_1_state = agent_1_state
        self.agent_2_state = agent_2_state

    def __str__(self):
        return '(' + str(self.type) + ', ' + self.agent_1 + ', ' + self.agent_2 + \
               ', ' + str(self.agent_1_state) + ', ' + str(self.agent_2_state) + ')'


class Constraints(object):
    def __init__(self):
        self.vertex_constraints = dict()
        self.edge_constraints = dict()

    def __str__(self):
        return "VC: " + str(self.vertex_constraints) + "EC: " + str(self.edge_constraints)

    def __eq__(self, other):
        return self.vertex_constraints == other.vertex_constraints and self.edge_constraints == other.edge_constraints

    def is_constrained(self, agent, current_vs, current_es):
        if current_vs in self.vertex_constraints.setdefault(agent, set()) or current_es in self.edge_constraints.setdefault(agent, set()):
            return True
        return False

    def add_vertex_constraint(self, agent, state):
        self.vertex_constraints.setdefault(agent, set()).add(state)

    def add_edge_constraint(self, agent, state):
        self.edge_constraints.setdefault(agent, set()).add(state)

    def get_goal_constraints(self, agent, t):
        vcs = set()
        for vc in self.vertex_constraints.setdefault(agent, set()):
            if vc.location == agent.goal and vc.time >= t:
                vcs.add(vc)
        return vcs


class HighLevelNode(object):
    def __init__(self):
        self.plan = Plan()
        self.constraints = Constraints()

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.constraints == other.constraints

    def __lt__(self, other):
        return self.plan.get_cost() < other.plan.get_cost()

    def update_solution(self, config, agent):
        path = astar.search(config, agent, self.constraints)
        if not path:
            return False
        self.plan.update_path(agent, path)
        return True

    def compute_solution(self, config, agents):
        for agent in agents:
            if not self.update_solution(config, agent):
                return False
        return True

    def get_first_conflict(self):
        if len(self.plan.agent_paths.keys()) < 2:
            return False
        for t in range(self.plan.get_max_t()):
            v_sts = {}
            e_sts = {}
            for agent, path in self.plan.agent_paths.items():
                v_st = self.plan.get_state(agent, t)
                e_st = self.plan.get_edge_state(agent, t)
                if v_st.get_conf_state() in v_sts.keys():
                    v_a = v_sts[v_st.get_conf_state()]
                    return Conflict(Conflict.VERTEX, agent, v_st, v_a, v_st.get_conf_state())
                else:
                    v_sts[v_st] = agent
                if e_st.get_conf_state() in e_sts.keys():
                    e_a = e_sts[e_st.get_conf_state()]
                    return Conflict(Conflict.EDGE, agent, e_st, e_a, e_st.get_conf_state())
                else:
                    e_sts[e_st] = agent
        return False

    def create_constraints_from_conflict(self, config, conflict):
        child1 = self
        child2 = deepcopy(self)
        if conflict.type == Conflict.VERTEX:
            child1.constraints.add_vertex_constraint(conflict.agent_1, conflict.agent_1_state)
            child2.constraints.add_vertex_constraint(conflict.agent_2, conflict.agent_2_state)

        elif conflict.type == Conflict.EDGE:
            child1.constraints.add_edge_constraint(conflict.agent_1, conflict.agent_1_state)
            child2.constraints.add_edge_constraint(conflict.agent_2, conflict.agent_2_state)

        has_solution1 = child1.update_solution(config, conflict.agent_1)
        has_solution2 = child2.update_solution(config, conflict.agent_2)
        children = []
        if has_solution1:
            children.append(child1)
        if has_solution2:
            children.append(child2)
        return children

    def get_first_conflict_x(self):
        if len(self.plan.agent_paths.keys()) < 2:
            return False
        for t in range(self.plan.get_max_t()):
            for agent_1, agent_2 in combinations(self.plan.agent_paths.keys(), 2):
                state_1a = self.plan.get_state(agent_1, t)
                state_1b = self.plan.get_state(agent_1, t + 1)
                state_2a = self.plan.get_state(agent_2, t)
                state_2b = self.plan.get_state(agent_2, t + 1)

                if state_1a == state_2a:
                    return Conflict(Conflict.VERTEX, agent_1, state_1a, agent_2, state_2a)

                if state_1a.location == state_2b.location and state_1b.location == state_2a.location:
                    return Conflict(Conflict.EDGE, agent_1, state_1a, agent_2, state_2a)
        return False

    def update_solution_1(self, parent_node, agent_to_update):
        constraints = self.constraint_dict.setdefault(agent_to_update, Constraints())
        sp_move_count = self.sp_move_counts[agent_to_update]
        path, wait = self.a_star.search_wait(agent_to_update, constraints, sp_move_count, 1.5)
        if not path:
            return False
        self.solution.update({agent_to_update: (path, wait)})
        self.cost = parent_node.cost + len(path) - len(parent_node.solution[agent_to_update][0])
        self.total_wait = parent_node.total_wait + wait - parent_node.solution[agent_to_update][1]


def search(grid, agents, max_cost=sys.maxsize, imt=None, cat=None):
    config = AStarConfig(grid, grid.manhattan_distance, imt, cat)
    open_set = []
    start = HighLevelNode()

    if not start.compute_solution(config, agents):
        return None
    open_set.append(start)

    while open_set:
        print(len(open_set))
        p = min(open_set)
        if p.plan.get_cost() > max_cost:
            break
        open_set.remove(p)
        conflict = p.get_first_conflict()
        if not conflict:
            print("solution found")
            return p.plan
        start_time = time.time()
        for child in p.create_constraints_from_conflict(config, conflict):
            if child not in open_set:
                open_set.append(child)
        print("--- %s seconds ---" % (time.time() - start_time))
    return {}


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("param", help="input file containing map and obstacles")
    parser.add_argument("output", help="output file with the schedule")
    args = parser.parse_args()

    # Read from input file
    with open(args.param, 'r') as param_file:
        try:
            param = yaml.load(param_file, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)

    dimension = param["map"]["dimensions"]
    obstacles = param["map"]["obstacles"]
    agents_data = param['agents']

    grid = Grid(dimension[0], dimension[1], obstacles)
    agents = AgentData.create(dimension, agents_data)

    # Searching
    plan = search(grid, agents)
    solution = plan.generate_plan(grid)
    if not solution:
        print(" Solution not found")
        return

    # Write to output file
    with open(args.output, 'r') as output_yaml:
        try:
            output = yaml.load(output_yaml, Loader=yaml.FullLoader)
        except yaml.YAMLError as exc:
            print(exc)

    output["schedule"] = solution
    output["cost"] = plan.get_cost()
    with open(args.output, 'w') as output_yaml:
        yaml.safe_dump(output, output_yaml)


if __name__ == "__main__":
    main()
