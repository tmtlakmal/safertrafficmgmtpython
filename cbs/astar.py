from gridworld import Grid, VertexState, EdgeState, Path

class AStarConfig(object):
    def __init__(self, grid: Grid, h, imt, cat):
        self.grid = grid
        self.h = h
        self.imt = imt
        self.cat = cat


class AStarNode(object):
    def __init__(self, state:VertexState, g_score, h_score, c_count, pred):
        self.state = state
        self.g_score = g_score
        self.h_score = h_score
        self.c_count = c_count
        self.pred = pred

    def f_score(self):
        return self.g_score + self.h_score

    def __eq__(self, other):
        return self.state == other.state

    def __hash__(self):
        return hash(self.state)

    def __lt__(self, other):
        if self.f_score() < other.f_score():
            return True
        elif self.f_score() > other.f_score():
            return False
        else:
            if self.c_count < other.c_count:
                return True
            elif self.c_count > other.c_count:
                return False
            else:
                return self.h_score < other.h_score


def reconstruct_path_x(came_from, current):
    all_states = [current]
    while current in came_from.keys():
        current = came_from[current]
        all_states.append(current)
    return Path(all_states[::-1])

def reconstruct_path(data, current):
    all_states = []
    while current:
        all_states.append(current)
        current = data[current].pred
    return Path(all_states[::-1])


def search(config: AStarConfig, agent, constraints):
    initial_state = VertexState(0, agent.start)
    step_cost = 1
    closed_set = set()
    open_set = {initial_state}
    #came_from = {}
    #g_score = {initial_state: 0}
    #f_score = {initial_state: config.h(agent.start, agent.goal)}
    #c_count = {initial_state: 0}
    data = {initial_state:AStarNode(initial_state, 0, config.h(agent.start, agent.goal), 0, None)}

    while open_set:
        #temp_dict = {open_item: f_score.setdefault(open_item, float("inf")) for open_item in open_set}
        #current = min(temp_dict, key=temp_dict.get)
        temp_set = {data[open_item] for open_item in open_set}
        current_node = min(temp_set)
        current = current_node.state

        if current.location == agent.goal and not constraints.get_goal_constraints(agent, current.time):
            return reconstruct_path(data, current)

        open_set -= {current}
        closed_set |= {current}

        for neighbor_location in config.grid.get_neighbours(current.location):
            neighbor = VertexState(current.time + 1, neighbor_location)
            es = EdgeState(current.time, current.location, neighbor_location)
            if constraints.is_constrained(agent, current, es):
                continue
            if neighbor in closed_set:
                continue
            if config.imt and config.imt.is_illegal(current, es):
                if neighbor.location == agent.goal:
                    return False
                continue

            tentative_g_score = current_node.g_score + step_cost

            if neighbor not in open_set:
                open_set |= {neighbor}
            else:
                if tentative_g_score >= data[neighbor].g_score:
                    continue
            #elif tentative_g_score >= g_score.setdefault(neighbor, float("inf")):
            #    continue

            #c_count[neighbor] = config.cat.get_conflict_count(agent, neighbor, es)
            #came_from[neighbor] = current
            #g_score[neighbor] = tentative_g_score
            #f_score[neighbor] = g_score[neighbor] + config.h(neighbor.location, agent.goal)
            c_count = 0
            if config.cat:
                c_count = config.cat.get_conflict_count(agent, neighbor, es)
            data[neighbor] = AStarNode(neighbor, tentative_g_score, config.h(neighbor.location, agent.goal), c_count, current)
    return False

def search_idf(config: AStarConfig, agent, group, constraints, group_constraints):
    initial_state = VertexState(0, agent.start)
    step_cost = 1
    closed_set = set()
    open_set = {initial_state}
    came_from = {}
    g_score = {initial_state: 0}
    f_score = {initial_state: config.h(agent.start, agent.goal)}

    while open_set:
        temp_dict = {open_item: f_score.setdefault(open_item, float("inf")) for open_item in open_set}
        current = min(temp_dict, key=temp_dict.get)

        if current.location == agent.goal and \
                not constraints.get_goal_constraints(agent, current.time) and \
                not group_constraints.get_goal_constraints(group, current.time):
            return reconstruct_path(came_from, current)

        open_set -= {current}
        closed_set |= {current}

        for neighbor_location in config.grid.get_neighbours(current.location):
            neighbor = VertexState(current.time + 1, neighbor_location)
            if constraints.is_constrained(agent, current, neighbor):
                continue
            if group_constraints.is_constrained(group, current, neighbor):
                continue
            if neighbor in closed_set:
                continue

            tentative_g_score = g_score.setdefault(current, float("inf")) + step_cost

            if neighbor not in open_set:
                open_set |= {neighbor}
            elif tentative_g_score >= g_score.setdefault(neighbor, float("inf")):
                continue

            came_from[neighbor] = current

            g_score[neighbor] = tentative_g_score
            f_score[neighbor] = g_score[neighbor] + config.h(neighbor.location, agent.goal)
    return False
