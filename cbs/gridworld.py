import enum

import numpy as np
from math import fabs


class LocationData(object):
    def __init__(self, x=-1, y=-1):
        self.x = x
        self.y = y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __hash__(self):
        return hash((self.x, self.y))

    def __str__(self):
        return str((self.x, self.y))


class AgentData(object):
    def __init__(self, name, start, goal):
        self.name = name
        self.start = start
        self.goal = goal

    def __eq__(self, other):
        return self.name == other.name

    def __str__(self):
        return str((self.name, self.start, self.goal))

    def __hash__(self):
        return hash(self.name)

    def get_start_state(self):
        return VertexState(0, self.start)

    @staticmethod
    def create(dimension, agents_data):
        agents = set()
        for agent in agents_data:
            idx = int(agent['name'].replace("agent", ""))
            start = agent['start'][1] * dimension[0] + agent['start'][0]
            goal = agent['goal'][1] * dimension[0] + agent['goal'][0]
            agents.add(AgentData(agent['name'], start, goal))
        return agents


class VertexState(object):
    def __init__(self, time, location):
        self.time = time
        self.location = location

    def __eq__(self, other):
        return self.time == other.time and self.location == other.location

    def __hash__(self):
        return hash((self.time, self.location))

    def __str__(self):
        return str((self.time, self.location))

    def get_conf_state(self):
        return self


class EdgeState(object):
    def __init__(self, time, location1, location2):
        self.time = time
        self.location1 = location1
        self.location2 = location2
        self.conf_state = None

    def __eq__(self, other):
        return self.time == other.time and self.location1 == other.location1 and self.location2 == other.location2

    def __hash__(self):
        return hash((self.time, self.location1, self.location2))

    def __str__(self):
        return str((self.time, self.location1, self.location2))

    def get_conf_state(self):
        if not self.conf_state:
            self.conf_state = EdgeState(self.time, self.location2, self.location1)
        return self.conf_state



class Directions(enum.Enum):
    FOUR_WAY = [(0, 0), (1, 0), (-1, 0), (0, -1), (0, 1)]
    EIGHT_WAY = [(0, 0), (1, 0), (-1, 0), (0, 1), (0, -1), (1, 1), (1, -1), (-1, 1), (-1, -1)]


class Grid(object):
    def __init__(self, dim_x, dim_y, obstacles, directions=Directions.FOUR_WAY):
        location_data = [None] * (dim_x * dim_y)
        neighbours = [None] * (dim_x * dim_y)
        for i in range(dim_x):
            for j in range(dim_y):
                n = []
                for dx, dy in directions.value:
                    x, y = i + dx, j + dy
                    if 0 <= x < dim_x and 0 <= y < dim_y:
                        if (x, y) not in obstacles:
                            n.append(y * dim_x + x)
                neighbours[j * dim_x + i] = n
                location_data[j * dim_x + i] = LocationData(i, j)
        self.neighbours = np.array(neighbours)
        self.location_data = np.array(location_data)

    def get_neighbours(self, location):
        return self.neighbours[location]

    def get_location_data(self, location):
        return self.location_data[location]

    def manhattan_distance(self, loc1, loc2):
        loc1_data = self.get_location_data(loc1)
        loc2_data = self.get_location_data(loc2)
        return fabs(loc1_data.x - loc2_data.x) + fabs(loc1_data.y - loc2_data.y)


class Path(object):
    def __init__(self, states):
        self.states = states

    def get_action_count(self):
        if self.states:
            return len(self.states) - 1
        return False

    def get_move_count(self):
        if self.states:
            locations = set()
            for state in self.states:
                locations.add(state.location)
            return len(locations) - 1
        return False

    def get_wait_count(self):
        if self.states:
            return self.get_action_count() - self.get_move_count()
        return False


class Plan(object):
    def __init__(self):
        self.agent_paths = {}

    def update_path(self, agent, path):
        self.agent_paths[agent] = path

    def generate_plan(self, grid):
        plan = {}
        for agent, path in self.agent_paths.items():
            agent_plan = []
            for state in path.states:
                location = grid.get_location_data(state.location)
                agent_plan.append({'t': state.time, 'x': location.x, 'y': location.y})
            plan[agent.name] = agent_plan
        return plan

    def get_state(self, agent, t) -> VertexState:
        states = self.agent_paths[agent].states
        if t < len(states):
            return states[t]
        else:
            return VertexState(t, states[-1].location)

    def get_edge_state(self, agent, t) -> EdgeState:
        s1 = self.get_state(agent, t)
        s2 = self.get_state(agent, t + 1)
        return EdgeState(t, s1.location, s2.location)

    def get_cost(self):
        return sum([len(path.states) for path in self.agent_paths.values()])

    def get_max_t(self):
        return max([len(path.states) for path in self.agent_paths.values()])

    def add_paths(self, plan):
        for agent in plan.agent_paths:
            self.update_path(agent, plan.agent_paths[agent])

    def get_conf_vertex_states(self, t):
        vs = set()
        for agent in self.agent_paths:
            vs.add(self.get_state(agent, t).get_conf_state())
        return vs

    def get_conf_edge_states(self, t):
        es = set()
        for agent in self.agent_paths:
            es.add(self.get_edge_state(agent, t).get_conf_state())
        return es

    def get_conf_vertex_state_dict(self, t):
        vs = dict()
        for agent in self.agent_paths:
            state = self.get_state(agent, t).get_conf_state()
            vs.setdefault(state, set()).add(agent)
        return vs

    def get_conf_edge_state_dict(self, t):
        es = dict()
        for agent in self.agent_paths:
            state = self.get_edge_state(agent, t).get_conf_state()
            es.setdefault(state, set()).add(agent)
        return es
